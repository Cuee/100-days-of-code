#include <iostream>
#include <string>
#include <termios.h>
#include <unistd.h>

using namespace std;

int getNumber() 
{
    int number;
    termios oldt;
    tcgetattr(STDIN_FILENO, &oldt);
    termios newt = oldt;
    newt.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    cin >> number;
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    return number;
}

string returnWinner(int player1, int player2, int player3){
    int average = (player1 + player2 + player3)/3;

    double twoThirds = (2 * average)/3;

    if (player1 == twoThirds){
        return "Player1 Wins!";
    }
    else if(player2 == twoThirds){
        return "Player2 Wins!";
    }
    else if(player3 == twoThirds){
        return "Player3 Wins!";
    }
    else{
        return "No player wins!";
    }
}

int main(int argc, char** argv){
    int player1, player2, player3;

    cout << "Hello and welcome to the game: Two thirds of Average." << "\n";

    cout << "Player1, please enter a number: " << endl;
    player1 = getNumber();

    cout << "Player2, please enter a number: " << endl;
    player2 = getNumber();

    cout << "Player3, please enter a number: " << endl;
    player3 = getNumber();

    cout << "The numbers are: " << endl;
    cout << player1;
    cout << " ";
    
    cout << player2;
    cout << " ";

    cout << player3 << endl;
    string winner = returnWinner(player1, player2, player3);
    cout << winner << endl;
    
    return 0;
}

