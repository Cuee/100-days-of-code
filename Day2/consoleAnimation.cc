#include <iostream>
#include <string>
#include <termios.h>
#include <unistd.h>

using namespace std;

void clearScreen(int numOfCharacters)
{
    for (int i = 0; i < numOfCharacters; i++)
    {
        cout << "\b";
    }
}

void show(string text)
{
    cout << text;
}

void hide(int value)
{
    clearScreen(value);
}

void blink()
{
    for (int i = 0; i < 4; i++)
    {
        cout << "Start" << endl;
        sleep(0.2);
        hide(5);
        sleep(1);
        show("\x1B[31mBlink\033[0m");
        show("\x1B[34ming\033[0m");
        show("\x1B[32mTexts\033[0m");
        show("\x1B[33mWith\033[0m");
        show("\x1B[34mC++\033[0m");
        show("\x1B[35m!\033[0m\n");
        show("End!");
        sleep(1);
        hide(4);
    }
}

int main()
{
    blink();
}